package com.example.michal.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void printToLogs(View view) {
        // Find first menu item TextView and print the text to the logs

        TextView textView1 = (TextView) findViewById(R.id.menu_item_1);
        String menuItem1 = textView1.getText().toString();
        textView1.setText(menuItem1);
        Log.v("Main activity", menuItem1);

        // Find second menu item TextView and print the text to the logs

        TextView textView2 = (TextView) findViewById(R.id.menu_item_2);
        String menuItem2 = textView2.getText().toString();
        textView2.setText(menuItem2);
        Log.v("Main activity", menuItem2);


        // Find third menu item TextView and print the text to the logs

        TextView textView3 = (TextView) findViewById(R.id.menu_item_3);
        String menuItem3 = textView3.getText().toString();
        textView3.setText(menuItem3);
        Log.v("Main activity", menuItem3);

    }
}
