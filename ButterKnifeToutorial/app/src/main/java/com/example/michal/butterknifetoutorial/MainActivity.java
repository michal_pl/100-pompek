package com.example.michal.butterknifetoutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.edit_text_view)
    EditText edytowalnePoleTextowe;

    @BindView(R.id.text_view)
    TextView poleTextowe;

    @BindView(R.id.pusty_button)
    Button wcisnijMnie;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        wcisnijMnie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edytowalnePoleTextowe.getText().toString();
                poleTextowe.setText("Hello " + name);
            }
        });

    }
}
